#!/bin/sh

CSRF_SRC=$(cat /tmp/csrf | grep Set-Cookie | cut -d \; -f 1 | cut -d \= -f2 ) || exit 1
CSRF=$(echo $CSRF_SRC | cut -d ' ' -f1)
SESSION=$(echo $CSRF_SRC | cut -d ' ' -f2)
echo "csrf=$CSRF"
echo "session=$SESSION"

MIDDLEWARE=$(cat /tmp/middleware | grep csrfmiddlewaretoken | grep value | tail -n 1 | cut -d \' -f 6)
echo "middleware=$MIDDLEWARE"

curl --connect-timeout 5 -k 'https://ciscowifi.beltelecom.by/disconnect/' -H 'Host: ciscowifi.beltelecom.by' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Referer: https://ciscowifi.beltelecom.by/connected/' -H "Cookie: safe=1; csrftoken=${CSRF}; sessionid=${SESSION}; _pk_id.2.6987=69c276a2ffdafbc5.1553592851.1.1553592970.1553592851.; _pk_ses.2.6987=*' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' --data '${MIDDLEWARE}"
